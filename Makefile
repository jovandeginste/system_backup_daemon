.PHONY: all clean test

all: test clean
	$(MAKE) build
	$(MAKE) install

test:
	rubocop

clean:
	-rm -v *.gem

build:
	gem build system_backup.gemspec

install:
	sudo gem install system_backup-*.gem
