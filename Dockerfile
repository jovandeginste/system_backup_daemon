FROM ruby

RUN apt-get update && apt-get -y install iputils-ping rsync btrfs-progs && apt-get clean
WORKDIR /app
COPY . .
RUN bundle install
ENTRYPOINT ["/app/bin/system-backup"]
CMD ["start"]
