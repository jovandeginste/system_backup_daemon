# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'system_backup'
  s.version     = '1.0.0'
  s.summary     = 'Service to backup your various systems'
  s.description = 'Backup your systems to a local BTRFS volume over rsync, with snapshots'
  s.authors     = ['Jo Vandeginste']
  s.email       = 'jo.vandeginste@gmail.com'
  s.files       = Dir['lib/**/*.rb'] + Dir['bin/*']
  s.executables << 'system-backup'
  s.homepage =
    'https://github.com/jovandeginste/system_backup_daemon'
  s.license = 'Apache-2.0'
  s.metadata['rubygems_mfa_required'] = 'true'
  s.required_ruby_version = '>= 3.0'

  s.add_runtime_dependency 'activesupport', '~> 7.0'
  s.add_runtime_dependency 'concurrent-ruby', '~> 1.0'
  s.add_runtime_dependency 'erb', '~> 4.0'
  s.add_runtime_dependency 'human_duration', '~> 2.0'
  s.add_runtime_dependency 'open3', '~> 0.1'
  s.add_runtime_dependency 'thor', '~> 1.2'
  s.add_runtime_dependency 'zeitwerk', '~> 2.6'
end
