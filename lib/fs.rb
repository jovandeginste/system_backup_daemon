# frozen_string_literal: true

require 'fileutils'

class FS
  attr_reader :dir, :fs_type

  def initialize(dir)
    @dir = dir
    return unless Dir.exist?(dir)

    read_stat
  end

  def read_stat
    @fs_type = syscall('stat', '--file-system', '--format', '%T', dir)
  end

  def create(subdir)
    raise "'#{dir}' is not btrfs" unless btrfs?

    syscall('btrfs', 'subvolume', 'create', File.join(dir, subdir))
  end

  def join(subdir)
    FS.new(File.join(to_s, subdir))
  end

  def touch
    FileUtils.touch(to_s)
  end

  def exist?
    Dir.exist?(to_s)
  end

  def writable?
    File.writable?(to_s)
  end

  def to_s
    dir
  end

  def info
    "directory '#{dir}' is type '#{fs_type}'"
  end

  def btrfs?
    fs_type == 'btrfs'
  end

  def snapshot_to(target_dir)
    raise "'#{dir}' is not btrfs" unless btrfs?

    syscall('btrfs', 'subvolume', 'snapshot', '-r', dir.to_s, target_dir.to_s)
  end

  private

  def syscall(*cmd)
    io = IO.popen(cmd)
    stdout = io.readlines.join.strip
    io.close
    stdout.strip
  end
end
