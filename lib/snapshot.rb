# frozen_string_literal: true

require 'date'

class Snapshot
  attr_accessor :name, :min_age, :min_keep, :hourly, :daily, :weekly, :monthly, :yearly

  def initialize(name, params = {})
    @name = name.to_s
    @min_keep = params.fetch(:min_keep, 10)
    @min_age = params.fetch(:min_age, 1).days
    @hourly = params.fetch(:hourly, 0)
    @daily = params.fetch(:daily, 6)
    @weekly = params.fetch(:weekly, 6)
    @monthly = params.fetch(:monthly, 4)
    @yearly = params.fetch(:yearly, 1)
  end

  def to_s
    name
  end

  def check_for_pruning(dir)
    output, status = Open3.capture2e("btrfs subvolume list -o -r -s #{dir}")
    raise output unless status.success?

    snapshots = output.split("\n")
                      .each_with_object([]) do |e, a|
      a << parse_snapshot(dir, e)
    end

    snapshots = snapshots.sort_by { |v| v[:time] }.reverse

    annotate_snapshots(snapshots)
    to_prune = snapshots.select { |s| s[:snapshots].empty? }

    {
      prune: to_prune,
      keep: snapshots - to_prune
    }
  end

  def show_config
    {
      name: name,
      hourly: hourly,
      daily: daily,
      weekly: weekly,
      monthly: monthly,
      yearly: yearly
    }
  end

  private

  def parse_snapshot(dir, line)
    # ID 275 gen 147 cgen 147 top level 264 otime 2022-12-26 18:37:28 path name/2022-12-26_18-37-28
    _, _, _, _, _, _, _, _, _, _, odate, otime, _, path = line.split
    time = begin
      DateTime.strptime("#{File.basename(path)} #{Time.now.zone}", '%Y-%m-%d_%H-%M-%S %z')
    rescue Date::Error
      DateTime.strptime("#{odate} #{otime} #{Time.now.zone}", '%Y-%m-%d %H:%M:%S %z')
    end

    {
      time: time.to_time,
      week: time.strftime('%-V'),
      hour: time.strftime('%F-%H'),
      day: time.strftime('%F'),
      month: time.strftime('%Y-%m'),
      year: time.strftime('%Y'),
      path: path,
      dir: dir,
      fullpath: dir.join(File.basename(path)),
      snapshots: []
    }
  end

  def annotate_snapshots(snapshots)
    snapshots.first(@min_keep).each_with_index do |s, i|
      s[:snapshots] << Match.new(key: 'min_keep', element: i + 1, max: @min_keep)
    end

    snapshots.each do |s|
      age = (Time.now - s[:time]).round
      next if age > @min_age

      s[:snapshots] << Match.new(key: 'min_age', element: age, max: @min_age)
    end

    annotate(snapshots, hourly, :hour)
    annotate(snapshots, daily, :day)
    annotate(snapshots, weekly, :week)
    annotate(snapshots, monthly, :month)
    annotate(snapshots, yearly, :year)
  end

  def annotate(snapshots, max, key)
    count = 0

    cur_s = snapshots.first
    snapshots.each do |s|
      break if count == max

      cur_s = s if s[key] == cur_s[key]
      next unless s[key] < cur_s[key]

      count += 1
      cur_s[:snapshots] << Match.new(key: key, element: count, max: max, value: cur_s[key])
      cur_s = s
    end

    return if count == max

    count += 1
    cur_s[:snapshots] << Match.new(key: key, element: count, max: max, value: cur_s[key])
  end

  class Match
    def initialize(key:, element:, max:, value: nil)
      @key = key
      @element = element
      @max = max
      @value = value
    end

    def to_s
      v = "#{@key}:#{@element}/#{@max}"
      return v unless @value

      "#{v}@#{@value}"
    end
  end
end
