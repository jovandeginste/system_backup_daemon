# frozen_string_literal: true

require 'human_duration'
require 'shellwords'

class Fileset
  attr_reader :name, :root, :includes, :excludes, :max_size

  def initialize(params = {})
    @name = params.fetch(:name, 'nameless').to_s
    @root = params.fetch(:root, nil)
    @max_size = params.fetch(:max_size, nil)
    @includes = Array(params.fetch(:includes, nil))
    @excludes = Array(params.fetch(:excludes, nil))
  end

  def to_s
    name
  end

  def show_config
    {
      name: name,
      root: root,
      max_size: max_size,
      including: includes.join(', '),
      excluding: excludes.join(', ')
    }
  end

  def perform_backup(host, connect_host, rsync_path, mode)
    rsync_command = rsync_command_for(host, connect_host, rsync_path, mode)
    host.loggers.debug ">> #{rsync_command}"

    start = Time.now

    rsync_result = run_rsync_command(host, rsync_command)

    stop = Time.now

    host.loggers.info "Rsync exit code: #{rsync_result.exitstatus}; this took: #{(stop - start).to_i.human_duration}"

    #   0      Success
    #   1      Syntax or usage error
    #   2      Protocol incompatibility
    #   3      Errors selecting input/output files, dirs
    #   4      Requested  action not supported: an attempt was made to manipulate 64-bit files
    #          on a platform that cannot support them; or an option was specified that is supported
    #          by the client and not by the server.
    #   5      Error starting client-server protocol
    #   6      Daemon unable to append to log-file
    #   10     Error in socket I/O
    #   11     Error in file I/O
    #   12     Error in rsync protocol data stream
    #   13     Errors with program diagnostics
    #   14     Error in IPC code
    #   20     Received SIGUSR1 or SIGINT
    #   21     Some error returned by waitpid()
    #   22     Error allocating core memory buffers
    #   23     Partial transfer due to error
    #   24     Partial transfer due to vanished source files
    #   25     The --max-delete limit stopped deletions
    #   30     Timeout in data send/receive
    #   35     Timeout waiting for daemon connection

    return if [0, 23, 24].include?(rsync_result)

    raise 'Rsync failed...'
  end

  private

  def scoped_includes(mode, connect_host)
    includes.map { |i| scoped_path(mode, connect_host, i) }
  end

  def scoped_path(mode, connect_host, path)
    case mode.to_sym
    when :local
      path
    when :linux
      "#{connect_host}:#{path}"
    when :windows
      "#{connect_host}::#{path}"
    end
  end

  def run_rsync_command(host, rsync_command)
    Open3.popen3(rsync_command) do |stdin, stdout, stderr, wait_thr|
      stdin.close

      # Read from stdout and stderr in real-time
      stdout_thread = Thread.new do
        stdout.each do |line|
          host.loggers.info line.chomp
        end
      end

      stderr_thread = Thread.new do
        stderr.each do |line|
          host.loggers.warn line.chomp
        end
      end

      # Wait for the command to complete
      wait_thr.join
      stdout_thread.join
      stderr_thread.join

      # Check the exit status of the command
      return wait_thr.value
    end
  end

  def rsync_command_for(host, connect_host, rsync_path, mode)
    params = default_rsync_params.merge(
      {
        rsync_path: rsync_path,
        max_size: max_size,
        exclude: excludes,
        include: scoped_includes(mode, connect_host)
      }
    )

    [
      '/usr/bin/rsync', *params.as_command_parameters,
      scoped_path(mode, connect_host, root), host.backup_current_directory
    ].shelljoin
  end

  def default_rsync_params
    {
      rsh: 'ssh -o BatchMode=yes',
      compress: true,
      delete: true,
      times: true,
      delete_excluded: true,
      relative: true,
      hard_links: true,
      human_readable: true,
      archive: true,
      itemize_changes: true
    }
  end
end
