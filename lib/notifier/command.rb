# frozen_string_literal: true

module Notifier
  class Command
    def self.initialize(cfg = {})
      return if cfg.nil?

      cfg.map do |key, value|
        type = value.fetch(:type, nil).to_s.downcase

        case type
        when 'gotify'
          Notifier::Gotify.new(key, value)
        else
          raise "Unsupported notifier '#{type}'"
        end
      end
    end
  end
end
