# frozen_string_literal: true

require 'uri'
require 'net/http'
require 'json'

module Notifier
  class Gotify
    attr_accessor :name, :url

    def initialize(name, params)
      @name = name
      u = params.fetch(:url, nil)
      raise "Notifier 'gotify' needs 'url' configuration" unless u

      @token = params.fetch(:token, nil)
      raise "Notifier 'gotify' needs 'token' configuration" unless u

      @url = URI(u)
    end

    def to_s
      "gotify notification to '#{url}'"
    end

    def inspect
      to_s
    end

    def show_config
      {
        name: name,
        type: 'gotify',
        url: url.to_s
      }
    end

    def notify(loggers, title, message, priority: 6)
      req = Net::HTTP.new(url.host, url.port)
      req.use_ssl = url.scheme == 'https'

      request = Net::HTTP::Post.new(File.join(url.path, "message?token=#{@token}"))
      request['accept'] = 'application/json'
      request['Content-Type'] = 'application/json'

      body = {
        title: title,
        message: message,
        priority: priority
      }
      request.body = body.to_json

      response = req.request(request)

      if response.is_a? Net::HTTPSuccess
        loggers.info "#{self}: successful"
        return
      end

      loggers.warn "#{self}: failed"
    end
  end
end
