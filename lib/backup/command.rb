# frozen_string_literal: true

module Backup
  class Command
    def self.initialize(configuration = {})
      type = configuration.fetch(:type, nil).to_s.downcase

      case type
      when 'rsync'
        Backup::Rsync.new(configuration)
      when 'ssh'
        Backup::SSH.new(configuration)
      when 'exec'
        Backup::Exec.new(configuration)
      else
        raise "unsupported backup command '#{type}'"
      end
    end
  end
end
