# frozen_string_literal: true

require 'human_duration'

module Backup
  class SSH
    attr_reader :command, :remote_user

    def initialize(params = {})
      @command = params.fetch(:command, nil)
      raise "SSH backup command needs a 'command'" unless @command

      @remote_user = params.fetch(:remote_user, nil)
    end

    def perform_backup(host, connect_host)
      cmd = command
      cmd.gsub!('__connect_host__', connect_host.host)
      cmd.gsub!('__backup_current_directory__', host.backup_current_directory.to_s)

      user_str = remote_user ? "-l #{remote_user}" : ''
      remote = remote_user ? "#{remote_user}@#{connect_host}" : connect_host

      host.loggers.info "Running command on #{remote}: #{cmd}"

      cmd = "ssh -o BatchMode=yes #{user_str} #{connect_host} #{command}"

      start = Time.now
      status = run(host, cmd)
      stop = Time.now

      host.loggers.info "Command exit code: #{status.exitstatus}; this took: #{(stop - start).to_i.human_duration}"
    end

    def run(host, cmd)
      Open3.popen3(cmd) do |stdin, stdout, stderr, wait_thr|
        stdin.close

        # Read from stdout and stderr in real-time
        stdout_thread = Thread.new do
          stdout.each do |line|
            host.loggers.info line.chomp
          end
        end

        stderr_thread = Thread.new do
          stderr.each do |line|
            host.loggers.warn line.chomp
          end
        end

        # Wait for the command to complete
        wait_thr.join
        stdout_thread.join
        stderr_thread.join

        # Check the exit status of the command
        return wait_thr.value
      end
    end

    def to_s
      "ssh (user: #{@user}; command: #{@command})"
    end

    def show_config
      self
    end
  end
end
