# frozen_string_literal: true

require 'human_duration'

module Backup
  class Exec
    attr_reader :command

    def initialize(params = {})
      @command = params.fetch(:command, nil)
      raise "Exec backup command needs a 'command'" unless @command
    end

    def perform_backup(host, connect_host)
      cmd = command
      cmd.gsub!('__connect_host__', connect_host.host)
      cmd.gsub!('__backup_current_directory__', host.backup_current_directory.to_s)

      host.loggers.info "Running command: #{cmd}"

      start = Time.now
      status = run(host, cmd)
      stop = Time.now

      host.loggers.info "Command exit code: #{status.exitstatus}; this took: #{(stop - start).to_i.human_duration}"
    end

    def run(host, cmd)
      Open3.popen3(cmd) do |stdin, stdout, stderr, wait_thr|
        stdin.close

        # Read from stdout and stderr in real-time
        stdout_thread = Thread.new do
          stdout.each do |line|
            host.loggers.info line.chomp
          end
        end

        stderr_thread = Thread.new do
          stderr.each do |line|
            host.loggers.warn line.chomp
          end
        end

        # Wait for the command to complete
        wait_thr.join
        stdout_thread.join
        stderr_thread.join

        # Check the exit status of the command
        return wait_thr.value
      end
    end

    def to_s
      "exec (command: #{@command})"
    end

    def show_config
      self
    end
  end
end
