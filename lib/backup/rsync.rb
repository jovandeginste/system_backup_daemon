# frozen_string_literal: true

module Backup
  class Rsync
    attr_reader :fileset, :rsync_path, :mode

    def initialize(params = {})
      fileset_cfg = params.fetch(:fileset, nil)
      @fileset = Fileset.new(fileset_cfg)
      @rsync_path = params.fetch(:rsync_path, nil)
      @mode = params.fetch(:mode, 'linux')
    end

    def perform_backup(host, connect_host)
      fileset.perform_backup(host, connect_host, rsync_path, mode)
    end

    def to_s
      "rsync (fileset: #{@fileset.name}; mode: #{@mode})"
    end

    def show_config
      {
        name: to_s,
        mode: mode,
        fileset: fileset.name,
        rsync_path: rsync_path
      }
    end
  end
end
