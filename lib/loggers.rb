# frozen_string_literal: true

class Loggers
  attr_accessor :loggers

  def initialize(*loggers)
    @loggers = []
    add(*loggers)
  end

  def add(*loggers)
    loggers.each do |l|
      @loggers.push(l)
    end
  end

  def method_missing(method_name, *args)
    if logger_methods.include?(method_name)
      @loggers.each do |l|
        l.send(method_name, *args)
      end
    else
      super
    end
  end

  def respond_to_missing?(method_name, include_private = false)
    logger_methods.include?(method_name) || super
  end

  private

  def logger_methods
    %i[info error warn fatal debug progname= level=]
  end
end
