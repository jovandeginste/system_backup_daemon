# frozen_string_literal: true

require 'thor'

class CLI < Thor
  class_option :verbose, type: :boolean

  desc 'start', 'start the backup engine'
  def start
    engine = Engine.new(verbose: options[:verbose])
    engine.run
  end

  desc 'listhosts', 'list all configured hosts'
  def listhosts
    engine = Engine.new(verbose: options[:verbose])
    puts 'Configured hosts:'
    engine.hosts.each do |name, host|
      puts "- #{name} (#{host.command})"
    end
  end

  desc 'info NAME', 'show information about NAME'
  def info(name)
    engine = Engine.new(verbose: options[:verbose])
    host = engine.find_host(name)

    puts host.show_config.to_yaml
  end

  desc 'perform NAME', 'perform a single backup for NAME'
  def perform(name)
    engine = Engine.new(verbose: options[:verbose])
    host = engine.find_host(name)

    host.perform_live_backup
  end

  desc 'prune NAME', 'prune snapshots for NAME'
  option :dryrun, type: :boolean
  def prune(name)
    engine = Engine.new(verbose: options[:verbose])
    host = engine.find_host(name)

    host.prune_snapshots(dryrun: options[:dryrun])
  end

  desc 'configtest', 'test the configuration'
  option :backtrace, type: :boolean
  def configtest
    Engine.new(verbose: options[:verbose])
    puts 'Configuration seems to be valid'
  rescue StandardError => e
    puts 'There seems to be an error with your configuration'
    puts e
    puts e.backtrace if options[:backtrace]
  end

  def self.exit_on_failure?
    true
  end
end
