# frozen_string_literal: true

require 'open3'

class Connect
  attr_reader :name, :ping, :host

  def initialize(name, ping: nil, host: nil)
    @name = name
    @ping = ping || name
    @host = host || name
  end

  def to_s
    @name
  end

  def ping?
    _output, status = Open3.capture2e(ping_command)
    status.success?
  end

  private

  def ping_command
    "ping -q -c 1 -W 1 #{host}"
  end
end
