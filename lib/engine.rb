# frozen_string_literal: true

require 'yaml'
require 'logger'
require 'fileutils'
require 'active_support/core_ext/hash/indifferent_access'
require 'concurrent-ruby'
require 'human_duration'

class Engine # rubocop:disable Metrics/ClassLength:
  attr_reader :verbose, :cycles, :snapshots, :hosts, :loggers, :meta_directory,
              :max_backup_threads, :host_defaults, :config_dir, :log_file, :executor

  def run
    loggers.info "Let's go!"

    hosts.each_value do |h|
      schedule_next_backup(h)
    end

    loop do
      loggers.info "Tasks scheduled: #{@executor.scheduled_task_count}; completed: #{@executor.completed_task_count}"
      sleep 1.hours
    end
  end

  def initialize(verbose: false)
    @verbose = verbose

    # Initialize default logging, to stderr
    initialize_loggers
    @config_dir = find_config_dir
    configure

    # Reinitialize logging, including file
    initialize_loggers

    initialize_executor
  end

  def initialize_executor
    @executor = Concurrent::ThreadPoolExecutor.new(
      max_threads: max_backup_threads
    )
  end

  def schedule_next_backup(host, minimum_delay: 10.seconds)
    n = host.next_backup
    d = (n - Time.now)
    d = minimum_delay if d < minimum_delay

    loggers.info "Scheduling next backup for '#{host.name}' at #{n} (in #{d.to_i.human_duration})"

    Concurrent::ScheduledTask.execute(d, executor: @executor) { do_backup_in_cycle(host) }
  end

  def do_backup_in_cycle(host)
    loggers.info "Starting backup cycle for '#{host.name}'"

    if host.perform_live_backup
      host.prune_snapshots

      schedule_next_backup(host)
    else
      loggers.warn "No live host for '#{host.name}' - checking again in 1 hour"

      schedule_next_backup(host, minimum_delay: 1.hours)
    end
  rescue StandardError => e
    loggers.error "Error while performing the backup cycle for '#{host.name}': #{e}"

    schedule_next_backup(host, minimum_delay: 1.hours)
  end

  def find_cycle(name)
    @cycles[name.to_sym] or raise "no cycle with name #{name}"
  end

  def find_snapshot(name)
    @snapshots[name.to_sym] or raise "no snapshot with name #{name}"
  end

  def find_host(name)
    @hosts[name.to_sym] or raise "no host with name #{name}"
  end

  private

  def initialize_loggers
    @loggers = Loggers.new(
      Logger.new($stderr),
      Logger.new(@log_file, 10, 100 * 1024 * 1024)
    )
    loggers.progname = 'engine'
    loggers.level = verbose ? 'debug' : 'info'
  end

  def configure
    loggers.info "Loading configuration from #{config_dir}"
    c = read_configuration_file('engine.yaml')

    configure_from(c)
    FileUtils.mkdir(meta_directory) unless File.directory?(meta_directory)

    self.cycles = c.fetch(:cycles, nil)
    self.snapshots = c.fetch(:snapshots, nil)
    self.hosts = c.fetch(:hosts, nil)
  end

  def configure_from(cfg)
    @meta_directory = cfg.fetch(:meta_directory, nil)
    @max_backup_threads = cfg.fetch(:max_backup_threads, nil)
    @host_defaults = cfg.fetch(:host_defaults, nil)
    @log_file = cfg.fetch(:log_file, nil)
  end

  def read_configuration_file(filename)
    file = File.join(config_dir, filename)
    content = YAMLUtils.parse_file(file)

    content.with_indifferent_access
  end

  def cycles=(cycles = {})
    @cycles = {}
    cycles.each do |k, v|
      @cycles[k.to_sym] = Cycle.new(self, k, v)
    rescue StandardError => e
      raise "Problem configuring cycle '#{k}: #{e}"
    end
  end

  def snapshots=(snapshots = {})
    @snapshots = {}
    snapshots.each do |k, v|
      @snapshots[k.to_sym] = Snapshot.new(k, v)
    rescue StandardError => e
      raise "Problem configuring snapshot '#{k}: #{e}"
    end
  end

  def hosts=(hosts = {})
    @hosts = {}
    hosts.each do |k, v|
      v[:meta_directory] ||= @meta_directory
      @hosts[k.to_sym] = Host.new(self, k, v)
    rescue StandardError => e
      raise "Problem configuring host '#{k}: #{e}"
    end
  end

  def find_config_dir
    paths = [
      'config', '~/.config/backups/', '/etc/backup'
    ]

    paths.map { |p| File.expand_path(p) }.find { |p| Dir.exist?(p) }
  end
end
