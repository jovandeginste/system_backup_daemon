# frozen_string_literal: true

require 'json'
require 'erb'
require 'fileutils'
require 'open3'
require 'concurrent-ruby'

class Host # rubocop:disable Metrics/ClassLength:
  class NoBackup
    def to_s
      'never'
    end

    def until_human
      'never'
    end

    def since_human
      'never'
    end
  end

  attr_reader :name, :connect, :cycle_name, :base_directory, :current_subdir,
              :snapshot_name, :snapshot, :meta_directory, :log_file, :engine, :cycle, :loggers,
              :command, :notifiers

  NEVER = NoBackup.new

  def initialize(engine, name, params = {})
    @name = name.to_s
    @engine = engine

    apply_defaults(params)
    configure_from(params)

    configure_loggers

    find_dependencies

    check_sanity

    loggers.debug "Configuration for #{self} parsed"
  end

  def annotated_snapshots
    snapshot.check_for_pruning(backup_root_directory)
  end

  def prune_snapshots(dryrun: false)
    as = annotated_snapshots

    prune_snapshots_show_keep(as[:keep])
    prune_snapshots_show_prune(as[:prune], dryrun: dryrun)
  end

  def prune_snapshots_show_prune(snapshots, dryrun: false)
    if snapshots.empty?
      loggers.info 'No snapshots to prune'
      return
    end

    loggers.warn 'Pruning snapshots:'
    snapshots.each do |s|
      loggers.warn "- [PRUNE] #{s[:fullpath]}"
      next if dryrun

      output, status = Open3.capture2e("btrfs subvolume delete --commit-after #{s[:fullpath]}")
      raise output unless status.success?

      loggers.info output
    end
  end

  def prune_snapshots_show_keep(snapshots)
    if snapshots.empty?
      loggers.info 'No snapshots to keep'
      return
    end

    loggers.info 'Keeping snapshots:'
    snapshots.each do |s|
      loggers.info "- [KEEP] #{s[:fullpath]} (#{s[:snapshots].join(', ')})"
    end
  end

  def apply_defaults(cfg)
    cfg[:meta_directory] ||= @engine.meta_directory
    @engine.host_defaults.each do |k, v|
      cfg[k] ||= v
    end
  end

  def configure_loggers
    @loggers = Loggers.new(
      Logger.new($stderr),
      Logger.new(@log_file, 10, 100 * 1024 * 1024)
    )
    loggers.progname = name
    loggers.level = engine.verbose ? 'debug' : 'info'
  end

  def find_dependencies
    @cycle = @engine.find_cycle(@cycle_name)
    @snapshot = @engine.find_snapshot(@snapshot_name)
  end

  def configure_from(cfg)
    configure_dirs(cfg)
    @cycle_name = cfg.fetch(:cycle, nil)
    @log_file = cfg.fetch(:log_file, nil) || File.join(@meta_directory.to_s, "#{self}.log")
    @rsync_path = cfg.fetch(:rsync_path, nil)
    @snapshot_name = cfg.fetch(:snapshot, nil)
    @command = Backup::Command.initialize(cfg.fetch(:command, nil))
    @notifiers = Notifier::Command.initialize(cfg.fetch(:notifications, nil))

    parse_connections cfg.fetch(:connect, nil)
  end

  def configure_dirs(cfg)
    @base_directory = FS.new(cfg.fetch(:base_directory, nil))
    @current_subdir = cfg.fetch(:current_subdir, nil)
    @meta_directory = FS.new(cfg.fetch(:meta_directory, nil))
  end

  def parse_connections(connect)
    case connect
    when String
      @connect = [Connect.new(connect)]
    when Array
      @connect = connect.map { |c| Connect.new(c) }
    when Hash
      @connect = connect.map { |key, value| Connect.new(key, *value) }
    end
  end

  def to_s
    name
  end

  def backup_root_directory
    base_directory.join(name)
  end

  def backup_current_directory
    backup_root_directory.join(current_subdir)
  end

  def lock_file
    File.join(meta_directory.to_s, "#{name}.lock")
  end

  def last_backup_file
    File.join(meta_directory.to_s, "#{name}.last")
  end

  def connect_names
    connect.collect(&:name)
  end

  def last_backup
    File.exist?(last_backup_file) ? File.mtime(last_backup_file) : NEVER
  end

  def next_backup
    cycle.next_from(last_backup)
  end

  def show_config
    cfg = {
      connect: connect_names,
      cycle: cycle.show_config,
      command: command.show_config,
      notifiers: notifiers.collect(&:show_config),
      snapshot: snapshot.show_config,
      'backing up to': {
        root: backup_root_directory.to_s,
        current: backup_current_directory.to_s
      },
      files: {
        'log file': log_file,
        'meta file': last_backup_file,
        'lock file': lock_file
      },
      'last backup': last_backup.to_s,
      'next backup': next_backup.to_s,
      'next backup (human)': next_backup.until_human,
      locked: locked?
    }

    cfg.keys_to_s
  end

  def find_live_host
    connect.find(&:ping?)
  end

  def check_sanity
    base_directory.create(name) unless backup_root_directory.exist?

    unless backup_root_directory.exist?
      raise "Host directory '#{backup_root_directory}' does not exist; please correct!"
    end

    backup_root_directory.create(current_subdir) unless backup_current_directory.exist?

    unless backup_current_directory.exist?
      raise "Host directory '#{backup_current_directory}' does not exist; please correct!"
    end

    unless backup_root_directory.writable?
      raise "Host directory '#{backup_root_directory}' not writable; please correct!"
    end

    nil
  end

  def locked?
    File.exist?(lock_file) and File.mtime(lock_file) > Time.now - 2.hours
  end

  def unlock
    return unless locked?

    FileUtils.rm lock_file
  end

  def lock
    FileUtils.touch lock_file
  end

  def timestamp_subdir(date)
    date_subdir = date.localtime.strftime('%Y-%m-%d_%H-%M-%S')
    backup_root_directory.join(date_subdir)
  end

  def perform_backup(connect_host)
    raise "host '#{connect_host}' is not one of my hosts" unless connect.include?(connect_host)
    raise "lock file '#{lock_file}' found" if locked?

    lock

    loggers.info "Performing backup: #{show_config.to_json}"

    backup_current_directory.touch
    command.perform_backup(self, connect_host)
  ensure
    unlock
  end

  def create_snapshot(date)
    loggers.info backup_current_directory.info
    date_dir = timestamp_subdir(date)
    snapshot = backup_current_directory.snapshot_to(date_dir)
    loggers.info snapshot
  end

  def notify(title, message)
    return unless notifiers

    notifiers.each do |n|
      n.notify loggers, title, message
    end
  end

  def touch_last_backup_file(mtime)
    FileUtils.touch(last_backup_file, mtime: mtime)

    message = success_message.result_with_hash(
      name: name, time: mtime, command: command, cycle: cycle
    )
    notify "Backup report for '#{name}'", message
  end

  def perform_live_backup # rubocop:disable Metrics/AbcSize
    time = Time.now
    l = last_backup
    d = cycle.deadline_from(l)
    loggers.info "Performing backup - last backup was #{l}; hosts: #{connect_names.join(', ')}"

    live = find_live_host
    unless live
      raise 'no live hosts - skipping till next cycle' if d < Time.now

      return false
    end

    loggers.info "Live host: #{live.name}"
    perform_backup(live)
    create_snapshot(time)
    touch_last_backup_file(time)

    loggers.info 'Backup succeeded.'
    true
  rescue StandardError => e
    message = fail_message.result_with_hash(
      name: name, error: e, last_backup: l, deadline: d,
      command: command, time: time, cycle: cycle
    )
    notify "Backup failed for '#{name}'", message

    raise e
  end

  def fail_message
    ERB.new <<~EO_MESSAGE
      The backup for '**<%= name %>**' has failed: <%= error %>

      > <%= error.backtrace.join("\n") %>

      - **last backup:** <%= last_backup %> (<%= last_backup.since_human %>)
      - **deadline:** <%= deadline %> (<%= deadline.until_human %>)
      - **started at:** <%= time %> (took: <%= time.since_human %>)
      - **command:** <%= command %>
      - **cycle:** <%= cycle %>
    EO_MESSAGE
  end

  def success_message
    ERB.new <<~EO_MESSAGE
      The backup for '**<%= name %>**' has completed

      - **started at:** <%= time %> (took: <%= time.since_human %>)
      - **command:** <%= command %>
      - **cycle:** <%= cycle %>
    EO_MESSAGE
  end
end
