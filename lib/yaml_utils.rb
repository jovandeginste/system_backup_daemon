# frozen_string_literal: true

class YAMLUtils
  def self.parse_file(file)
    content = YAML.parse_file(file)
    YAMLUtils.parse_includes(content.root, file)

    content.to_ruby
  end

  def self.parse_includes(nodes, file)
    dir = File.dirname(file)

    nodes.children.each_with_index do |node, index|
      # Check if the node is a scalar with an "include" tag
      parse_includes(node, file) if node.is_a?(Psych::Nodes::Mapping)
      next unless node.is_a?(Psych::Nodes::Scalar) && node.tag == '!include'

      file = File.expand_path(File.join(dir, node.value))
      # Read the file specified in the value
      included_data = YAML.parse_file(file)
      parse_includes(included_data.root, file)

      nodes.children[index] = included_data
    end
  end
end
