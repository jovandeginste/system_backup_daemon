# frozen_string_literal: true

require 'human_duration'

class Time
  def until_human
    d = (self - Time.now).to_i

    return "#{d.abs.human_duration} ago" if d.negative?

    d.human_duration
  end

  def since_human
    d = (Time.now - self).to_i

    return "#{d.abs.human_duration} ago" if d.negative?

    d.human_duration
  end
end
