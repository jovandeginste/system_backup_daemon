# frozen_string_literal: true

class String
  def blank?
    nil? or self == ''
  end
end
