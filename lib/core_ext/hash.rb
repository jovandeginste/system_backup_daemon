# frozen_string_literal: true

class Hash
  def keys_to_s
    transform_keys(&:to_s)
      .transform_values do |v|
        case v
        when Hash
          v.keys_to_s
        when Array
          v.map { |e| e.is_a?(Hash) ? e.keys_to_s : e }
        else
          v
        end
      end
  end

  def as_command_parameters
    parameters = []
    each do |key, values|
      key = key.to_s.gsub('_', '-')
      case values
      when NilClass, FalseClass
        # do nothing
      when TrueClass
        parameters.push("--#{key}")
      when Array
        values.each do |v|
          parameters.push("--#{key}=#{v}")
        end
      else
        parameters.push("--#{key}=#{values}")
      end
    end

    parameters
  end
end
