# frozen_string_literal: true

class Object
  def arrayfy
    [self].flatten.compact
  end

  def not_blank?
    !blank?
  end

  def not_nil?
    !nil?
  end

  def blank?
    nil?
  end
end
