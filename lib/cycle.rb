# frozen_string_literal: true

require 'human_duration'

class Cycle
  attr_accessor :name, :min_age, :max_age, :engine

  def initialize(engine, name, params = {})
    @name = name.to_s
    @engine = engine
    @min_age = params.fetch(:min_age, 1).days
    @max_age = params.fetch(:max_age, 2).days
  end

  def to_s
    name
  end

  def deadline_from(last_time)
    return Time.now - 1 if last_time == Host::NEVER

    last_time + max_age
  end

  def next_from(last_time)
    return Time.now - 1 if last_time == Host::NEVER

    last_time + min_age
  end

  def show_config
    {
      name: name,
      'min age': min_age.human_duration,
      'max age': max_age.human_duration
    }
  end
end
